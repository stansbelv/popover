import React, { useState, useRef } from "react";
import { useOutsideAlerter } from "../hooks/useOutsideAlerter";
import { P } from "./styles";

export interface PopoverProps {
  triggerEvent?: "click" | "hover";
  trigger?: React.ReactNode;
  position?: "top" | "bottom";
  alignment?: "left" | "right";
  width?: "small" | "medium" | "large";
}

const Popover: React.FC<PopoverProps> = ({
  triggerEvent,
  trigger,
  children,
  position,
  width,
  alignment,
}) => {
  const [isShown, setIsShown] = useState<boolean>(false);
  const node = useRef<HTMLDivElement>(null);

  useOutsideAlerter(node, () => setIsShown(false));

  const handleOnMouseEnter = () => triggerEvent === "hover" && setIsShown(true);

  const handleOnMouseLeave = () =>
    triggerEvent === "hover" && setIsShown(false);

  const handleOnClick = <T extends Element>(e: React.MouseEvent<T>) => {
    e.stopPropagation();
    triggerEvent !== "hover" && setIsShown(!isShown);
  };

  return (
    <P.Container ref={node}>
      <P.Trigger
        onClick={handleOnClick}
        onMouseEnter={handleOnMouseEnter}
        onMouseLeave={handleOnMouseLeave}
      >
        {trigger}
        {isShown && (
          <P.Content position={position} width={width} alignment={alignment}>
            <P.Inner>{children}</P.Inner>
          </P.Content>
        )}
      </P.Trigger>
    </P.Container>
  );
};

export default Popover;
