import styled from "styled-components";
import { PopoverProps } from "./Popover";

const Container = styled.div`
  position: relative;
`;

const Inner = styled.div`
  position: relative;
  border-radius: 4px;
  background: #fff;
  box-shadow: 0 0 0 1px #e4e4e4, 0 4px 11px #e4e4e4;
  cursor: auto;
`;

const Trigger = styled.div`
  cursor: pointer;
  position: relative;
  width: fit-content;
`;

const Content = styled.div`
  position: absolute;
  top: ${(props: PopoverProps) => props.position === "bottom" && "100%"};
  padding-top: ${(props: PopoverProps) =>
    props.position === "bottom" && "1rem"};
  padding-bottom: ${(props: PopoverProps) =>
    props.position === "top" && "1rem"};
  bottom: ${(props: PopoverProps) => props.position === "top" && "100%"};
  right: ${(props: PopoverProps) => props.alignment === "left" && "0"};
  left: ${(props: PopoverProps) => props.alignment === "right" && "0"};
  min-width: ${(props: PopoverProps) => props.width === "small" && "12rem"};
  min-width: ${(props: PopoverProps) => props.width === "medium" && "15rem"};
  min-width: ${(props: PopoverProps) => props.width === "large" && "20rem"};
`;

export const P = {
  Container,
  Content,
  Inner,
  Trigger,
};
