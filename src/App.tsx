import React from "react";

import "./App.css";
import Popover from "./components/Popover";
import styled from "styled-components";

const Content = styled.div`
  padding: 1rem;
  border-radius: 4px;
  background: #fff;
  box-shadow: 0 0 0 1px #e3e3e3, 0 4px 11px #e3e3e3;
  cursor: auto;
`;

const Button = styled.button`
  display: inline-block;
  color: #fff;
  background: palevioletred;
  padding: 0.5rem 1.5rem;
  border: 1px solid palevioletred;
  border-radius: 4px;
  display: block;
  cursor: pointer;
`;

const Container = styled.div`
  display: flex;
  width: 840px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  justify-content: space-between;
`;

function App() {
  return (
    <div className="App">
      <Container>
        <Popover
          position="bottom"
          alignment="right"
          width="medium"
          trigger={<Button>Hover</Button>}
          triggerEvent="hover"
        >
          <Content> Popover content</Content>
        </Popover>
        <Popover
          position="bottom"
          alignment="left"
          width="medium"
          trigger={<Button>Hover</Button>}
          triggerEvent="hover"
        >
          <Content> Popover content</Content>
        </Popover>
        <Popover
          position="top"
          alignment="right"
          width="medium"
          trigger={<Button>Hover</Button>}
          triggerEvent="hover"
        >
          <Content> Popover content</Content>
        </Popover>
        <Popover
          position="top"
          alignment="left"
          width="medium"
          trigger={<Button>Hover</Button>}
          triggerEvent="hover"
        >
          <Content> Popover content</Content>
        </Popover>
        <Popover
          position="bottom"
          alignment="right"
          width="medium"
          trigger={<Button>Click</Button>}
          // triggerEvent="hover"
        >
          <Content>Popover content</Content>
        </Popover>
      </Container>
    </div>
  );
}

export default App;
